$(window).scrollTop(0,0);

var winHeight = 0;
var adjustHeight = 0;
var maxHeight = 0;
var menOffsetTop = 0;

var fold_2_start = 0;
var fold_3_start = 0;
var fold_4_start = 0;
var fold_5_start = 0;
var fold_6_start = 0;
var fold_7_start = 0;
var fold_8_start = 0;

var fold_2_count = -2;
var fold_2_max_count = 35;
var fold_2_top = 0;

var fold_3_count = -2;
var fold_3_max_count = 72;
var fold_3_top = 0;

var fold_4_count = -2;
var fold_4_max_count = 57;
var fold_4_top = 0;

var fold_5_count = -2;
var fold_5_max_count = 105;
var fold_5_top = 0;

var fold_6_count = -2;
var fold_6_max_count = 129;
var fold_6_top = 0;

var fold_7_count = -2;
var fold_7_max_count = 150;
var fold_7_top = 0;

var fold_8_count = -2;
var fold_8_max_count = 157;
var fold_8_top = 0;

$(window).on('load', function () {
	$(window).scrollTop(0,0);
	setTimeout(function(){
		$('.loader').fadeOut(500);	
		setPosition();
		pageScroll();
		
	}, 2000);
	loadAllImages();

	setPosition();
	
	if(navigator.userAgent.indexOf('Mac') > 0){
		$('body').addClass('mac-os');
	}
	
	$('.menu-toggle').click(function(e) {
		if(!$(this).hasClass('toggled-on')){
			$(this).addClass('toggled-on');	
			$('header nav').addClass('active');	
		}else {
			$(this).removeClass('toggled-on');	
			$('header nav').removeClass('active');	
		}
	});
	$('.scroll-link').click(function(e) {
		var body = $("html, body");
		body.stop().animate({scrollTop:fold_2_top}, 1000, 'swing', function() { });
	});
	
	
	$('.learn-more-link').click(function(e) {
		e.preventDefault();
		var relElem = $(this).attr('rel');
		$('.popup-mask').fadeIn(1000,function(){
			$('#'+relElem).fadeIn(100);
		});
	});
	$('.popup-close').click(function(e) {
		$('.popup-mask').fadeOut(1000,function(){
			$('.popup-inner-wrap').fadeOut(100);
		});
	});
	
	
});
$(window).on('resize', function () {
	setPosition();
});
function setPosition(){
	winHeight = $('.fold').eq(1).height()+(($('.fold').eq(1).height()-$(window).height())/2);
	adjustHeight = ($('.fold').eq(1).height()-$(window).height())/2;
	maxHeight = $("#fold_8").offset().top+adjustHeight+10;
	
	
	$('.men-animation').css({'left':$('.currency').offset().left+100});
	menOffsetTop = $('.men-animation').position().top;
	
	fold_2_start = Math.round(winHeight/2);
	fold_3_start = Math.round((winHeight/2)+winHeight);
	fold_4_start = Math.round((winHeight/2)+(winHeight*2));
	fold_5_start = Math.round((winHeight/2)+(winHeight*3));
	fold_6_start = Math.round((winHeight/2)+(winHeight*4));
	fold_7_start = Math.round((winHeight/2)+(winHeight*5));
	fold_8_start = Math.round((winHeight/2)+(winHeight*6));
	
	fold_2_top = Math.round($("#fold_2").offset().top+adjustHeight);
	fold_3_top = Math.round($("#fold_3").offset().top+adjustHeight);
	fold_4_top = Math.round($("#fold_4").offset().top+adjustHeight);
	fold_5_top = Math.round($("#fold_5").offset().top+adjustHeight);
	fold_6_top = Math.round($("#fold_6").offset().top+adjustHeight);
	fold_7_top = Math.round($("#fold_7").offset().top+adjustHeight);
	fold_8_top = Math.round($("#fold_8").offset().top+adjustHeight);
}
$(window).on('scroll', function () {
	pageScroll();
	
});
function loadAllImages(){
	$('img').each(function(index, element) {
        if($(this).attr('img-url')){
			$(this).attr('src',	$(this).attr('img-url'));
		}
    });	
}

var windowScrollBoolean = true;
var scrollableElement = window;

window.addEventListener('wheel', findScrollDirectionOtherBrowsers);

function findScrollDirectionOtherBrowsers(event){
	var delta;
	
	if (event.wheelDelta){
		delta = event.wheelDelta;
	}else{
		delta = -1 * event.deltaY;
	}
	
	if (delta < 0){
		//console.log("DOWN");
		windowScrollBoolean = true;
		
	
	}else if (delta > 0){
		windowScrollBoolean = false;
		//console.log("UP");
		
	}
	
}
scrollableElement.addEventListener('keydown', function ( event ) {
	var keyName = event.keyCode;
	if(keyName === 40){
	 windowScrollBoolean = true;
	}
	if(keyName === 38){
	 windowScrollBoolean = false;
	}
});	
scrollableElement.addEventListener('scroll', function ( event ) {
	if(windowScrollBoolean){
		
		if($(window).scrollTop() >= fold_2_top){
			if(fold_2_count <= fold_2_max_count){
				
				window.scrollTo(0,fold_2_top);
				fold_2_count++;
				fold_2_anim(fold_2_count);
			}
		}
		if($(window).scrollTop() >= fold_3_top){
			if(fold_3_count <= fold_3_max_count){
				window.scrollTo(0,fold_3_top);
				fold_3_count++;
				fold_3_anim(fold_3_count);
			}
		}
		if($(window).scrollTop() >= fold_4_top){
			if(fold_4_count <= fold_4_max_count){
				window.scrollTo(0,fold_4_top);
				fold_4_count++;
				fold_4_anim(fold_4_count);
			}
		}
		if($(window).scrollTop() >= fold_5_top){
			if(fold_5_count <= fold_5_max_count){
				window.scrollTo(0,fold_5_top);
				fold_5_count++;
				fold_5_anim(fold_5_count);
			}
		}
		if($(window).scrollTop() >= fold_6_top){
			if(fold_6_count <= fold_6_max_count){
				window.scrollTo(0,fold_6_top);
				fold_6_count++;
				fold_6_anim(fold_6_count);
			}
		}
		if($(window).scrollTop() >= fold_7_top){
			if(fold_7_count <= fold_7_max_count){
				window.scrollTo(0,fold_7_top);
				fold_7_count++;
				fold_7_anim(fold_7_count);
			}
		}
		if($(window).scrollTop() >= fold_8_top){
			if(fold_8_count <= fold_8_max_count){
				window.scrollTo(0,fold_8_top);
				fold_8_count++;
				fold_8_anim(fold_8_count);
			}
		}
	}else if(!windowScrollBoolean){
		//console.log($(window).scrollTop()+'   '+fold_2_top+'    '+fold_2_count);
		if($(window).scrollTop() < fold_2_top){
			
		}
		if($(window).scrollTop() <= fold_2_top){
			if(fold_2_count >= 0){
				window.scrollTo(0,fold_2_top);
				fold_2_count--;
				fold_2_anim(fold_2_count);
			}
		}
		if($(window).scrollTop() <= fold_3_top){
			if(fold_3_count >= 0){
				window.scrollTo(0,fold_3_top);
				fold_3_count--;
				fold_3_anim(fold_3_count);
			}
		}
		if($(window).scrollTop() <= fold_4_top){
			if(fold_4_count >= 0){
				window.scrollTo(0,fold_4_top);
				fold_4_count--;
				fold_4_anim(fold_4_count);
			}
		}
		if($(window).scrollTop() <= fold_5_top){
			if(fold_5_count >= 0){
				window.scrollTo(0,fold_5_top);
				fold_5_count--;
				fold_5_anim(fold_5_count);
			}
		}
		if($(window).scrollTop() <= fold_6_top){
			if(fold_6_count >= 0){
				window.scrollTo(0,fold_6_top);
				fold_6_count--;
				fold_6_anim(fold_6_count);
			}
		}
		if($(window).scrollTop() <= fold_7_top){
			if(fold_7_count >= 0){
				window.scrollTo(0,fold_7_top);
				fold_7_count--;
				fold_7_anim(fold_7_count);
			}
		}
		if($(window).scrollTop() <= fold_8_top){
			if(fold_8_count >= 0){
				window.scrollTo(0,fold_8_top);
				fold_8_count--;
				fold_8_anim(fold_8_count);
			}
		}
	}
});
function pageScroll(){
	if($(window).scrollTop() == 0){
		$('.fold-1, .fold-2').removeClass('active');
	}
	
	if($(window).scrollTop() <= 0){
		$('.fold-1').removeClass('active');
	}
	if($(window).scrollTop() >= 0 && $(window).scrollTop() <= 20){
		$('.fold-1').removeClass('active');
		$('.fold-1').eq(0).addClass('active');
	}
	if($(window).scrollTop() >= 21 && $(window).scrollTop() <= 40){
		$('.fold-1').removeClass('active');
		$('.fold-1').eq(1).addClass('active');
	}
	if($(window).scrollTop() >= 41 && $(window).scrollTop() <= 60){
		$('.fold-1').removeClass('active');
		$('.fold-1').eq(2).addClass('active');
	}
	if($(window).scrollTop() >= 61 && $(window).scrollTop() <= 80){
		$('.fold-1').removeClass('active');
		$('.fold-1').eq(3).addClass('active');
	}
	if($(window).scrollTop() >= 81 && $(window).scrollTop() <= 100){
		$('.fold-1').removeClass('active');
		$('.fold-1').eq(4).addClass('active');
	}
	if($(window).scrollTop() >= 101 && $(window).scrollTop() <= 120){
		$('.fold-1').removeClass('active');
		$('.fold-1').eq(5).addClass('active');
	}
	if($(window).scrollTop() >= 121 && $(window).scrollTop() <= 140){
		$('.fold-1').removeClass('active');
		$('.fold-1').eq(6).addClass('active');
	}
	if($(window).scrollTop() >= 141 && $(window).scrollTop() <= 160){
		$('.fold-1').removeClass('active');
		$('.fold-1').eq(7).addClass('active');
	}
	if($(window).scrollTop() >= 161 && $(window).scrollTop() <= 180){
		$('.fold-1').removeClass('active');
		$('.fold-1').eq(8).addClass('active');
	}
	if($(window).scrollTop() >= 181 && $(window).scrollTop() <= 200){
		$('.fold-1').removeClass('active');
		$('.fold-1').eq(9).addClass('active');
	}
	if($(window).scrollTop() >= 201 && $(window).scrollTop() <= 220){
		$('.fold-1').removeClass('active');
		$('.fold-1').eq(10).addClass('active');
	}
	if($(window).scrollTop() >= 221 && $(window).scrollTop() <= 240){
		$('.fold-1').removeClass('active');
		$('.fold-1').eq(11).addClass('active');
	}
	
	if($(window).scrollTop() <= 0 ){
		$('.men-animation').css({'-moz-transform': 'scale(.4)','-webkit-transform': 'scale(.4)','-o-transform': 'scale(.4)','-ms-transform': 'scale(.4)','transform': 'scale(.4)'});	
	}
	if($(window).scrollTop() >= 0 && $(window).scrollTop() <= 30){
		$('.men-animation').css({'-moz-transform': 'scale(.4)','-webkit-transform': 'scale(.4)','-o-transform': 'scale(.4)','-ms-transform': 'scale(.4)','transform': 'scale(.4)'});	
	}
	if($(window).scrollTop() >= 31 && $(window).scrollTop() <= 50){
		$('.men-animation').css({'-moz-transform': 'scale(.425)','-webkit-transform': 'scale(.425)','-o-transform': 'scale(.425)','-ms-transform': 'scale(.425)','transform': 'scale(.425)'});	
	}
	if($(window).scrollTop() >= 51 && $(window).scrollTop() <= 70){
		$('.men-animation').css({'-moz-transform': 'scale(.45)','-webkit-transform': 'scale(.45)','-o-transform': 'scale(.45)','-ms-transform': 'scale(.45)','transform': 'scale(.45)'});	
	}
	if($(window).scrollTop() >= 71 && $(window).scrollTop() <= 90){
		$('.men-animation').css({'-moz-transform': 'scale(.475)','-webkit-transform': 'scale(.475)','-o-transform': 'scale(.475)','-ms-transform': 'scale(.475)','transform': 'scale(.475)'});	
	}
	if($(window).scrollTop() >= 91 && $(window).scrollTop() <= 110){
		$('.men-animation').css({'-moz-transform': 'scale(.5)','-webkit-transform': 'scale(.5)','-o-transform': 'scale(.5)','-ms-transform': 'scale(.5)','transform': 'scale(.5)'});	
	}
	if($(window).scrollTop() >= 111 && $(window).scrollTop() <= 130){
		$('.men-animation').css({'-moz-transform': 'scale(.525)','-webkit-transform': 'scale(.525)','-o-transform': 'scale(.525)','-ms-transform': 'scale(.525)','transform': 'scale(.525)'});	
	}
	if($(window).scrollTop() >= 131 && $(window).scrollTop() <= 150){
		$('.men-animation').css({'-moz-transform': 'scale(.55)','-webkit-transform': 'scale(.55)','-o-transform': 'scale(.55)','-ms-transform': 'scale(.55)','transform': 'scale(.55)'});	
	}
	if($(window).scrollTop() >= 151 && $(window).scrollTop() <= 170){
		$('.men-animation').css({'-moz-transform': 'scale(.575)','-webkit-transform': 'scale(.575)','-o-transform': 'scale(.575)','-ms-transform': 'scale(.575)','transform': 'scale(.575)'});	
	}
	if($(window).scrollTop() >= 171 && $(window).scrollTop() <= 190){
		$('.men-animation').css({'-moz-transform': 'scale(.6)','-webkit-transform': 'scale(.6)','-o-transform': 'scale(.6)','-ms-transform': 'scale(.6)','transform': 'scale(.6)'});	
	}
	if($(window).scrollTop() >= 191 && $(window).scrollTop() <= 210){
		$('.men-animation').css({'-moz-transform': 'scale(.625)','-webkit-transform': 'scale(.625)','-o-transform': 'scale(.625)','-ms-transform': 'scale(.625)','transform': 'scale(.625)'});	
	}
	if($(window).scrollTop() >= 211 && $(window).scrollTop() <= 230){
		$('.men-animation').css({'-moz-transform': 'scale(.65)','-webkit-transform': 'scale(.65)','-o-transform': 'scale(.65)','-ms-transform': 'scale(.65)','transform': 'scale(.65)'});	
	}
	if($(window).scrollTop() >= 231 && $(window).scrollTop() <= 250){
		$('.men-animation').css({'-moz-transform': 'scale(.675)','-webkit-transform': 'scale(.675)','-o-transform': 'scale(.675)','-ms-transform': 'scale(.675)','transform': 'scale(.675)'});	
	}
	if($(window).scrollTop() >= 251 && $(window).scrollTop() <= 270){
		$('.men-animation').css({'-moz-transform': 'scale(.7)','-webkit-transform': 'scale(.7)','-o-transform': 'scale(.7)','-ms-transform': 'scale(.7)','transform': 'scale(.7)'});	
	}
	if($(window).scrollTop() >= 271 && $(window).scrollTop() <= 290){
		$('.men-animation').css({'-moz-transform': 'scale(.725)','-webkit-transform': 'scale(.725)','-o-transform': 'scale(.725)','-ms-transform': 'scale(.725)','transform': 'scale(.725)'});	
	}
	if($(window).scrollTop() >= 291 && $(window).scrollTop() <= 310){
		$('.men-animation').css({'-moz-transform': 'scale(.75)','-webkit-transform': 'scale(.75)','-o-transform': 'scale(.75)','-ms-transform': 'scale(.75)','transform': 'scale(.75)'});	
	}
	if($(window).scrollTop() >= 311 && $(window).scrollTop() <= 330){
		$('.men-animation').css({'-moz-transform': 'scale(.775)','-webkit-transform': 'scale(.775)','-o-transform': 'scale(.775)','-ms-transform': 'scale(.775)','transform': 'scale(.775)'});	
	}
	if($(window).scrollTop() >= 331 && $(window).scrollTop() <= 350){
		$('.men-animation').css({'-moz-transform': 'scale(.8)','-webkit-transform': 'scale(.8)','-o-transform': 'scale(.8)','-ms-transform': 'scale(.8)','transform': 'scale(.8)'});	
	}
	if($(window).scrollTop() >= 351 && $(window).scrollTop() <= 370){
		$('.men-animation').css({'-moz-transform': 'scale(.825)','-webkit-transform': 'scale(.825)','-o-transform': 'scale(.825)','-ms-transform': 'scale(.825)','transform': 'scale(.825)'});	
	}
	if($(window).scrollTop() >= 371 && $(window).scrollTop() <= 390){
		$('.men-animation').css({'-moz-transform': 'scale(.85)','-webkit-transform': 'scale(.85)','-o-transform': 'scale(.85)','-ms-transform': 'scale(.85)','transform': 'scale(.85)'});	
	}
	if($(window).scrollTop() >= 391 && $(window).scrollTop() <= 410){
		$('.men-animation').css({'-moz-transform': 'scale(.875)','-webkit-transform': 'scale(.875)','-o-transform': 'scale(.875)','-ms-transform': 'scale(.875)','transform': 'scale(.875)'});	
	}
	if($(window).scrollTop() >= 411 && $(window).scrollTop() <= 430){
		$('.men-animation').css({'-moz-transform': 'scale(.9)','-webkit-transform': 'scale(.9)','-o-transform': 'scale(.9)','-ms-transform': 'scale(.9)','transform': 'scale(.9)'});	
	}
	if($(window).scrollTop() >= 431 && $(window).scrollTop() <= 450){
		$('.men-animation').css({'-moz-transform': 'scale(.925)','-webkit-transform': 'scale(.925)','-o-transform': 'scale(.925)','-ms-transform': 'scale(.925)','transform': 'scale(.925)'});	
	}
	if($(window).scrollTop() >= 451 && $(window).scrollTop() <= 470){
		$('.men-animation').css({'-moz-transform': 'scale(.95)','-webkit-transform': 'scale(.95)','-o-transform': 'scale(.95)','-ms-transform': 'scale(.95)','transform': 'scale(.95)'});	
	}
	if($(window).scrollTop() >= 471 && $(window).scrollTop() <= 490){
		$('.men-animation').css({'-moz-transform': 'scale(.975)','-webkit-transform': 'scale(.975)','-o-transform': 'scale(.975)','-ms-transform': 'scale(.975)','transform': 'scale(.975)'});	
	}
	if($(window).scrollTop() >= 491 && $(window).scrollTop() <= 510){
		$('.men-animation').css({'-moz-transform': 'scale(1)','-webkit-transform': 'scale(1)','-o-transform': 'scale(1)','-ms-transform': 'scale(1)','transform': 'scale(1)'});	
	}
	if($(window).scrollTop() >= 510){
		$('.men-animation').css({'-moz-transform': 'scale(1)','-webkit-transform': 'scale(1)','-o-transform': 'scale(1)','-ms-transform': 'scale(1)','transform': 'scale(1)'});	
	}
	
	
	// FLIGHT
	if($(window).scrollTop() <= (fold_2_top-220)){
		$('.flight').css({'left':0});
	}
	if($(window).scrollTop() >= (fold_2_top-220) && $(window).scrollTop() <= (fold_2_top-201)){
		$('.flight').css({'left':0});
	}
	if($(window).scrollTop() >= (fold_2_top-200) && $(window).scrollTop() <= (fold_2_top-181)){
		$('.flight').css({'left':45});
	}
	if($(window).scrollTop() >= (fold_2_top-180) && $(window).scrollTop() <= (fold_2_top-161)){
		$('.flight').css({'left':90});
	}
	if($(window).scrollTop() >= (fold_2_top-160) && $(window).scrollTop() <= (fold_2_top-141)){
		$('.flight').css({'left':135});
	}
	if($(window).scrollTop() >= (fold_2_top-140) && $(window).scrollTop() <= (fold_2_top-121)){
		$('.flight').css({'left':180});
	}
	if($(window).scrollTop() >= (fold_2_top-120) && $(window).scrollTop() <= (fold_2_top-101)){
		$('.flight').css({'left':225});
	}
	if($(window).scrollTop() >= (fold_2_top-100) && $(window).scrollTop() <= (fold_2_top-81)){
		$('.flight').css({'left':270});
	}
	if($(window).scrollTop() >= (fold_2_top-80) && $(window).scrollTop() <= (fold_2_top-61)){
		$('.flight').css({'left':315});
	}
	if($(window).scrollTop() >= (fold_2_top-60) && $(window).scrollTop() <= (fold_2_top-41)){
		$('.flight').css({'left':360});
	}
	if($(window).scrollTop() >= (fold_2_top-40) && $(window).scrollTop() <= (fold_2_top-21)){
		$('.flight').css({'left':405});
	}
	if($(window).scrollTop() >= (fold_2_top-10) && $(window).scrollTop() <= (fold_2_top-1)){
		$('.flight').css({'left':450});
	}
	
	
	
	// BUILD HANDLE
	if($(window).scrollTop() <= (fold_4_top-220)){
		$('.build-handle').css({'left':-100});
	}
	if($(window).scrollTop() >= (fold_4_top-220) && $(window).scrollTop() <= (fold_4_top-201)){
		$('.build-handle').css({'left':-90});
	}
	if($(window).scrollTop() >= (fold_4_top-200) && $(window).scrollTop() <= (fold_4_top-181)){
		$('.build-handle').css({'left':-80});
	}
	if($(window).scrollTop() >= (fold_4_top-180) && $(window).scrollTop() <= (fold_4_top-161)){
		$('.build-handle').css({'left':-70});
	}
	if($(window).scrollTop() >= (fold_4_top-160) && $(window).scrollTop() <= (fold_4_top-141)){
		$('.build-handle').css({'left':-60});
	}
	if($(window).scrollTop() >= (fold_4_top-140) && $(window).scrollTop() <= (fold_4_top-121)){
		$('.build-handle').css({'left':-50});
	}
	if($(window).scrollTop() >= (fold_4_top-120) && $(window).scrollTop() <= (fold_4_top-101)){
		$('.build-handle').css({'left':-40});
	}
	if($(window).scrollTop() >= (fold_4_top-100) && $(window).scrollTop() <= (fold_4_top-81)){
		$('.build-handle').css({'left':-30});
	}
	if($(window).scrollTop() >= (fold_4_top-80) && $(window).scrollTop() <= (fold_4_top-61)){
		$('.build-handle').css({'left':-20});
	}
	if($(window).scrollTop() >= (fold_4_top-60) && $(window).scrollTop() <= (fold_4_top-41)){
		$('.build-handle').css({'left':-10});
	}
	if($(window).scrollTop() >= (fold_4_top-40) && $(window).scrollTop() <= (fold_4_top-21)){
		$('.build-handle').css({'left':0});
	}
	if($(window).scrollTop() >= (fold_4_top-10) && $(window).scrollTop() <= (fold_4_top-1)){
		$('.build-handle').css({'left':10});
	}

	
	
	// CAR
	if($(window).scrollTop() <= (fold_5_top-220)){
		$('.car').css({'left':530});
	}
	if($(window).scrollTop() >= (fold_5_top-220) && $(window).scrollTop() <= (fold_5_top-201)){
		$('.car').css({'left':535});
	}
	if($(window).scrollTop() >= (fold_5_top-200) && $(window).scrollTop() <= (fold_5_top-181)){
		$('.car').css({'left':540});
	}
	if($(window).scrollTop() >= (fold_5_top-180) && $(window).scrollTop() <= (fold_5_top-161)){
		$('.car').css({'left':545});
	}
	if($(window).scrollTop() >= (fold_5_top-160) && $(window).scrollTop() <= (fold_5_top-141)){
		$('.car').css({'left':550});
	}
	if($(window).scrollTop() >= (fold_5_top-140) && $(window).scrollTop() <= (fold_5_top-121)){
		$('.car').css({'left':555});
	}
	if($(window).scrollTop() >= (fold_5_top-120) && $(window).scrollTop() <= (fold_5_top-101)){
		$('.car').css({'left':560});
	}
	if($(window).scrollTop() >= (fold_5_top-100) && $(window).scrollTop() <= (fold_5_top-81)){
		$('.car').css({'left':570});
	}
	if($(window).scrollTop() >= (fold_5_top-80) && $(window).scrollTop() <= (fold_5_top-61)){
		$('.car').css({'left':575});
	}
	if($(window).scrollTop() >= (fold_5_top-60) && $(window).scrollTop() <= (fold_5_top-41)){
		$('.car').css({'left':580});
	}
	if($(window).scrollTop() >= (fold_5_top-40) && $(window).scrollTop() <= (fold_5_top-21)){
		$('.car').css({'left':585});
	}
	if($(window).scrollTop() >= (fold_5_top-10) && $(window).scrollTop() <= (fold_5_top-1)){
		$('.car').css({'left':590});
	}
	

	//fold_2_anim();
	//fold_3_anim();
	//fold_4_anim();
	//fold_5_anim();
	//fold_6_anim();
	//fold_7_anim();
	//fold_8_anim();
}
function fold_2_anim(val) {
	
	if(val <= 0){
		$('.fold-2').removeClass('active');
	}
	if(val >= 0 && val <= 3){
		$('.fold-2').removeClass('active');
		$('.fold-2').eq(0).addClass('active');
	}
	if(val >= 4 && val <= 6){
		$('.fold-2').removeClass('active');
		$('.fold-2').eq(1).addClass('active');
	}
	if(val >= 7 && val <= 9){
		$('.fold-2').removeClass('active');
		$('.fold-2').eq(2).addClass('active');
	}
	if(val >= 10 && val <= 12){
		$('.fold-2').removeClass('active');
		$('.fold-2').eq(3).addClass('active');
	}
	if(val >= 13 && val <= 15){
		$('.fold-2').removeClass('active');
		$('.fold-2').eq(4).addClass('active');
	}
	if(val >= 16 && val <= 18){
		$('.fold-2').removeClass('active');
		$('.fold-2').eq(5).addClass('active');
	}
	if(val >= 19 && val <= 21){
		$('.fold-2').removeClass('active');
		$('.fold-2').eq(6).addClass('active');
	}
	if(val >= 22 && val <= 24){
		$('.fold-2').removeClass('active');
		$('.fold-2').eq(7).addClass('active');
	}
	if(val >= 25 && val <= 27){
		$('.fold-2').removeClass('active');
		$('.fold-2').eq(8).addClass('active');
	}


	
	
	
	// FLIGHT
	if(val >= 0 && val <= 5){
		$('.flight').css({'left':450});
	}
	if(val >= 6 && val <= 10){
		$('.flight').css({'left':500});
	}
	if(val >= 11 && val <= 15){
		$('.flight').css({'left':550});
	}
	if(val >= 16 && val <= 20){
		$('.flight').css({'left':600});
	}
	if(val >= 21 && val <= 25){
		$('.flight').css({'left':650});
	}
	if(val >= 26 && val <= 30){
		$('.flight').css({'left':700});
	}
	
	if(val >= 30){
		$('.flight').css({'left':700});
	}
}
function fold_3_anim(val) {
	
		
	if(val <= 0){
		$('.fold-3').removeClass('active');
	}
	if(val >= 0 && val <= 3){
		$('.fold-3').removeClass('active');
		$('.fold-3').eq(0).addClass('active');
	}
	if(val >= 4 && val <= 6){
		$('.fold-3').removeClass('active');
		$('.fold-3').eq(1).addClass('active');
	}
	if(val >= 7 && val <= 9){
		$('.fold-3').removeClass('active');
		$('.fold-3').eq(2).addClass('active');
	}
	if(val >= 10 && val <= 12){
		$('.fold-3').removeClass('active');
		$('.fold-3').eq(3).addClass('active');
	}
	if(val >= 13 && val <= 15){
		$('.fold-3').removeClass('active');
		$('.fold-3').eq(4).addClass('active');
	}
	if(val >= 16 && val <= 18){
		$('.fold-3').removeClass('active');
		$('.fold-3').eq(5).addClass('active');
	}
	if(val >= 19 && val <= 21){
		$('.fold-3').removeClass('active');
		$('.fold-3').eq(6).addClass('active');
	}
	if(val >= 22 && val <= 24){
		$('.fold-3').removeClass('active');
		$('.fold-3').eq(7).addClass('active');
	}
	if(val >= 25 && val <= 27){
		$('.fold-3').removeClass('active');
		$('.fold-3').eq(8).addClass('active');
	}
	if(val >= 28 && val <= 30){
		$('.fold-3').removeClass('active');
		$('.fold-3').eq(9).addClass('active');
	}
	if(val >= 31 && val <= 33){
		$('.fold-3').removeClass('active');
		$('.fold-3').eq(10).addClass('active');
	}


	
	// COMFORT
	if(val <= 0){
		$('.comfort-img').removeClass('active');
        $('.comfort-img').eq(0).addClass('active');
	}
	if(val >= 0 && val <= 3){
		$('.comfort-img').removeClass('active');
		$('.comfort-img').eq(0).addClass('active');
	}
	if(val >= 4 && val <= 6){
		$('.comfort-img').removeClass('active');
		$('.comfort-img').eq(1).addClass('active');
	}
	if(val >= 7 && val <= 9){
		$('.comfort-img').removeClass('active');
		$('.comfort-img').eq(2).addClass('active');
	}
	if(val >= 10 && val <= 12){
		$('.comfort-img').removeClass('active');
		$('.comfort-img').eq(3).addClass('active');
	}
	if(val >= 13 && val <= 15){
		$('.comfort-img').removeClass('active');
		$('.comfort-img').eq(4).addClass('active');
	}
	if(val >= 16 && val <= 18){
		$('.comfort-img').removeClass('active');
		$('.comfort-img').eq(5).addClass('active');
	}
	if(val >= 19 && val <= 21){
		$('.comfort-img').removeClass('active');
		$('.comfort-img').eq(6).addClass('active');
	}
	if(val >= 22 && val <= 24){
		$('.comfort-img').removeClass('active');
		$('.comfort-img').eq(7).addClass('active');
	}
	if(val >= 25 && val <= 27){
		$('.comfort-img').removeClass('active');
		$('.comfort-img').eq(8).addClass('active');
	}
	if(val >= 28 && val <= 30){
		$('.comfort-img').removeClass('active');
		$('.comfort-img').eq(9).addClass('active');
	}
	if(val >= 31 && val <= 33){
		$('.comfort-img').removeClass('active');
		$('.comfort-img').eq(10).addClass('active');
	}
	if(val >= 34 && val <= 36){
		$('.comfort-img').removeClass('active');
		$('.comfort-img').eq(11).addClass('active');
	}
	if(val >= 37 && val <= 39){
		$('.comfort-img').removeClass('active');
		$('.comfort-img').eq(12).addClass('active');
	}
	if(val >= 40 && val <= 42){
		$('.comfort-img').removeClass('active');
		$('.comfort-img').eq(13).addClass('active');
	}
	if(val >= 43 && val <= 45){
		$('.comfort-img').removeClass('active');
		$('.comfort-img').eq(14).addClass('active');
	}
	if(val >= 46 && val <= 48){
		$('.comfort-img').removeClass('active');
		$('.comfort-img').eq(15).addClass('active');
	}
	if(val >= 49 && val <= 51){
		$('.comfort-img').removeClass('active');
		$('.comfort-img').eq(16).addClass('active');
	}
	if(val >= 52 && val <= 54){
		$('.comfort-img').removeClass('active');
		$('.comfort-img').eq(17).addClass('active');
	}
	if(val >= 54){
		$('.comfort-img').removeClass('active');
		$('.comfort-img').eq(17).addClass('active');
	}
	
	
	// TIME
	if(val <= 0){
		$('.time-img').removeClass('active');
        $('.time-img').eq(0).addClass('active');
	}
	if(val >= 0 && val <= 3){
		$('.time-img').removeClass('active');
		$('.time-img').eq(0).addClass('active');
	}
	if(val >= 4 && val <= 6){
		$('.time-img').removeClass('active');
		$('.time-img').eq(1).addClass('active');
	}
	if(val >= 7 && val <= 9){
		$('.time-img').removeClass('active');
		$('.time-img').eq(2).addClass('active');
	}
	if(val >= 10 && val <= 12){
		$('.time-img').removeClass('active');
		$('.time-img').eq(3).addClass('active');
	}
	if(val >= 13 && val <= 15){
		$('.time-img').removeClass('active');
		$('.time-img').eq(4).addClass('active');
	}
	if(val >= 16 && val <= 18){
		$('.time-img').removeClass('active');
		$('.time-img').eq(5).addClass('active');
	}
	if(val >= 19 && val <= 21){
		$('.time-img').removeClass('active');
		$('.time-img').eq(6).addClass('active');
	}
	if(val >= 22 && val <= 24){
		$('.time-img').removeClass('active');
		$('.time-img').eq(7).addClass('active');
	}
	if(val >= 25 && val <= 27){
		$('.time-img').removeClass('active');
		$('.time-img').eq(8).addClass('active');
	}
	if(val >= 28 && val <= 30){
		$('.time-img').removeClass('active');
		$('.time-img').eq(9).addClass('active');
	}
	if(val >= 31 && val <= 33){
		$('.time-img').removeClass('active');
		$('.time-img').eq(10).addClass('active');
	}
	if(val >= 34 && val <= 36){
		$('.time-img').removeClass('active');
		$('.time-img').eq(11).addClass('active');
	}
	if(val >= 37 && val <= 39){
		$('.time-img').removeClass('active');
		$('.time-img').eq(12).addClass('active');
	}
	if(val >= 40 && val <= 42){
		$('.time-img').removeClass('active');
		$('.time-img').eq(13).addClass('active');
	}
	if(val >= 43 && val <= 45){
		$('.time-img').removeClass('active');
		$('.time-img').eq(14).addClass('active');
	}
	if(val >= 46 && val <= 48){
		$('.time-img').removeClass('active');
		$('.time-img').eq(15).addClass('active');
	}
	if(val >= 49 && val <= 51){
		$('.time-img').removeClass('active');
		$('.time-img').eq(16).addClass('active');
	}
	if(val >= 52 && val <= 54){
		$('.time-img').removeClass('active');
		$('.time-img').eq(17).addClass('active');
	}
	if(val >= 55 && val <= 57){
		$('.time-img').removeClass('active');
		$('.time-img').eq(18).addClass('active');
	}
	if(val >= 58 && val <= 60){
		$('.time-img').removeClass('active');
		$('.time-img').eq(19).addClass('active');
	}

	if(val >= 60){
		$('.time-img').removeClass('active');
		$('.time-img').eq(19).addClass('active');
	}
	
	
	// OBJECTIVE
	if(val <= 0){
		$('.objective-img').removeClass('active');
        $('.objective-img').eq(0).addClass('active');
	}
	if(val >= 0 && val <= 3){
		$('.objective-img').removeClass('active');
		$('.objective-img').eq(0).addClass('active');
	}
	if(val >= 4 && val <= 6){
		$('.objective-img').removeClass('active');
		$('.objective-img').eq(1).addClass('active');
	}
	if(val >= 7 && val <= 9){
		$('.objective-img').removeClass('active');
		$('.objective-img').eq(2).addClass('active');
	}
	if(val >= 10 && val <= 12){
		$('.objective-img').removeClass('active');
		$('.objective-img').eq(3).addClass('active');
	}
	if(val >= 13 && val <= 15){
		$('.objective-img').removeClass('active');
		$('.objective-img').eq(4).addClass('active');
	}
	if(val >= 16 && val <= 18){
		$('.objective-img').removeClass('active');
		$('.objective-img').eq(5).addClass('active');
	}
	if(val >= 19 && val <= 21){
		$('.objective-img').removeClass('active');
		$('.objective-img').eq(6).addClass('active');
	}
	if(val >= 22 && val <= 24){
		$('.objective-img').removeClass('active');
		$('.objective-img').eq(7).addClass('active');
	}
	if(val >= 25 && val <= 27){
		$('.objective-img').removeClass('active');
		$('.objective-img').eq(8).addClass('active');
	}
	if(val >= 28 && val <= 30){
		$('.objective-img').removeClass('active');
		$('.objective-img').eq(9).addClass('active');
	}
	if(val >= 31 && val <= 33){
		$('.objective-img').removeClass('active');
		$('.objective-img').eq(10).addClass('active');
	}
	if(val >= 34 && val <= 36){
		$('.objective-img').removeClass('active');
		$('.objective-img').eq(11).addClass('active');
	}
	if(val >= 37 && val <= 39){
		$('.objective-img').removeClass('active');
		$('.objective-img').eq(12).addClass('active');
	}
	if(val >= 40 && val <= 42){
		$('.objective-img').removeClass('active');
		$('.objective-img').eq(13).addClass('active');
	}
	if(val >= 43 && val <= 45){
		$('.objective-img').removeClass('active');
		$('.objective-img').eq(14).addClass('active');
	}
	if(val >= 46 && val <= 48){
		$('.objective-img').removeClass('active');
		$('.objective-img').eq(15).addClass('active');
	}
	if(val >= 49 && val <= 51){
		$('.objective-img').removeClass('active');
		$('.objective-img').eq(16).addClass('active');
	}
	if(val >= 52 && val <= 54){
		$('.objective-img').removeClass('active');
		$('.objective-img').eq(17).addClass('active');
	}
	if(val >= 55 && val <= 57){
		$('.objective-img').removeClass('active');
		$('.objective-img').eq(18).addClass('active');
	}
	if(val >= 58 && val <= 60){
		$('.objective-img').removeClass('active');
		$('.objective-img').eq(19).addClass('active');
	}
	if(val >= 61 && val <= 63){
		$('.objective-img').removeClass('active');
		$('.objective-img').eq(20).addClass('active');
	}
	if(val >= 64 && val <= 66){
		$('.objective-img').removeClass('active');
		$('.objective-img').eq(21).addClass('active');
	}
	if(val >= 67 && val <= 69){
		$('.objective-img').removeClass('active');
		$('.objective-img').eq(22).addClass('active');
	}
	if(val >= 70 && val <= 72){
		$('.objective-img').removeClass('active');
		$('.objective-img').eq(23).addClass('active');
	}

	if(val >= 72){
		$('.objective-img').removeClass('active');
		$('.objective-img').eq(23).addClass('active');
	}

	
}
function fold_4_anim(val) {
//	$('.build-handle-img').eq(0).addClass('active');
		
	if(val <= 0){
		$('.fold-4').removeClass('active');
	}
	if(val >= 1 && val <= 3){
		$('.fold-4').removeClass('active');
		$('.fold-4').eq(0).addClass('active');
	}
	if(val >= 4 && val <= 6){
		$('.fold-4').removeClass('active');
		$('.fold-4').eq(1).addClass('active');
	}
	if(val >= 7 && val <= 9){
		$('.fold-4').removeClass('active');
		$('.fold-4').eq(2).addClass('active');
	}
	if(val >= 10 && val <= 12){
		$('.fold-4').removeClass('active');
		$('.fold-4').eq(3).addClass('active');
	}
	if(val >= 13 && val <= 15){
		$('.fold-4').removeClass('active');
		$('.fold-4').eq(4).addClass('active');
	}
	if(val >= 16 && val <= 18){
		$('.fold-4').removeClass('active');
		$('.fold-4').eq(5).addClass('active');
	}
	if(val >= 19 && val <= 21){
		$('.fold-4').removeClass('active');
		$('.fold-4').eq(6).addClass('active');
	}
	if(val >= 22 && val <= 24){
		$('.fold-4').removeClass('active');
		$('.fold-4').eq(7).addClass('active');
	}
	if(val >= 25 && val <= 27){
		$('.fold-4').removeClass('active');
		$('.fold-4').eq(8).addClass('active');
	}
	if(val >= 28 && val <= 30){
		$('.fold-4').removeClass('active');
		$('.fold-4').eq(9).addClass('active');
	}
	if(val >= 31 && val <= 33){
		$('.fold-4').removeClass('active');
		$('.fold-4').eq(10).addClass('active');
	}
	if(val >= 34 && val <= 36){
		$('.fold-4').removeClass('active');
		$('.fold-4').eq(11).addClass('active');
	}


	
	// BUILD
	if(val >= 0 && val <= 3){
		$('.build-1').css({'bottom':-120});
		$('.build-3').css({'bottom':-80});
		
	}
	if(val >= 4 && val <= 6){
		$('.build-1').css({'bottom':-100});
		$('.build-3').css({'bottom':-60});
	}
	if(val >= 7 && val <= 9){
		$('.build-1').css({'bottom':-80});
		$('.build-3').css({'bottom':-50});
	}
	if(val >= 10 && val <= 12){
		$('.build-1').css({'bottom':-60});
		$('.build-3').css({'bottom':-40});
	}
	if(val >= 13 && val <= 15){
		$('.build-1').css({'bottom':-40});
		$('.build-3').css({'bottom':-20});
	}
	if(val >= 16 && val <= 18){
		$('.build-1').css({'bottom':-20});
		$('.build-3').css({'bottom':-10});
	}
	if(val >= 19 && val <= 21){
		$('.build-1').css({'bottom':0});
		$('.build-3').css({'bottom':0});
	}
	if(val >= 21){
		$('.build-1').css({'bottom':0});
		$('.build-3').css({'bottom':0});
	}
	
	
	// BUILD HANDLE ANIMATION
	if(val <= 0){
		$('.build-handle-img').eq(0).addClass('active');
	}
	if(val >= 0 && val <= 3){
		$('.build-handle-img').removeClass('active');
		$('.build-handle-img').eq(0).addClass('active');
	}
	if(val >= 4 && val <= 6){
		$('.build-handle-img').removeClass('active');
		$('.build-handle-img').eq(1).addClass('active');
	}
	if(val >= 7 && val <= 9){
		$('.build-handle-img').removeClass('active');
		$('.build-handle-img').eq(2).addClass('active');
	}
	if(val >= 10 && val <= 12){
		$('.build-handle-img').removeClass('active');
		$('.build-handle-img').eq(3).addClass('active');
	}
	if(val >= 13 && val <= 15){
		$('.build-handle-img').removeClass('active');
		$('.build-handle-img').eq(4).addClass('active');
	}
	if(val >= 16 && val <= 18){
		$('.build-handle-img').removeClass('active');
		$('.build-handle-img').eq(5).addClass('active');
	}
	if(val >= 19 && val <= 21){
		$('.build-handle-img').removeClass('active');
		$('.build-handle-img').eq(6).addClass('active');
	}
	if(val >= 22 && val <= 24){
		$('.build-handle-img').removeClass('active');
		$('.build-handle-img').eq(7).addClass('active');
	}
	if(val >= 25 && val <= 27){
		$('.build-handle-img').removeClass('active');
		$('.build-handle-img').eq(8).addClass('active');
	}
	if(val >= 28 && val <= 30){
		$('.build-handle-img').removeClass('active');
		$('.build-handle-img').eq(9).addClass('active');
	}
	if(val >= 31 && val <= 33){
		$('.build-handle-img').removeClass('active');
		$('.build-handle-img').eq(10).addClass('active');
	}
	if(val >= 34 && val <= 36){
		$('.build-handle-img').removeClass('active');
		$('.build-handle-img').eq(11).addClass('active');
	}
	if(val >= 37 && val <= 39){
		$('.build-handle-img').removeClass('active');
		$('.build-handle-img').eq(12).addClass('active');
	}
	if(val >= 40 && val <= 42){
		$('.build-handle-img').removeClass('active');
		$('.build-handle-img').eq(13).addClass('active');
	}
	if(val >= 43 && val <= 45){
		$('.build-handle-img').removeClass('active');
		$('.build-handle-img').eq(14).addClass('active');
	}
	if(val >= 46 && val <= 48){
		$('.build-handle-img').removeClass('active');
		$('.build-handle-img').eq(15).addClass('active');
	}
	if(val >= 49 && val <= 51){
		$('.build-handle-img').removeClass('active');
		$('.build-handle-img').eq(16).addClass('active');
	}
	if(val >= 52 && val <= 54){
		$('.build-handle-img').removeClass('active');
		$('.build-handle-img').eq(17).addClass('active');
	}
	if(val >= 55 && val <= 57){
		$('.build-handle-img').removeClass('active');
		$('.build-handle-img').eq(18).addClass('active');
	}

	if(val >= 57){
		$('.build-handle-img').removeClass('active');
		$('.build-handle-img').eq(18).addClass('active');
	}


	// BUILD HANDLE
	if(val <= 0){
		$('.build-handle').css({'left':10});
	}
	if(val >= 0 && val <= 3){
		$('.build-handle').css({'left':30});
	}
	if(val >= 4 && val <= 6){
		$('.build-handle').css({'left':60});
	}
	if(val >= 7 && val <= 9){
		$('.build-handle').css({'left':90});
	}
	if(val >= 10 && val <= 12){
		$('.build-handle').css({'left':120});
	}
	if(val >= 13 && val <= 15){
		$('.build-handle').css({'left':150});
	}
	if(val >= 15){
		$('.build-handle').css({'left':150});
	}
	
}
function fold_5_anim(val) {
	
		
	if(val <= 0){
		$('.fold-5').removeClass('active');
	}
	if(val >= 1 && val <= 54){
		$('.fold-5').removeClass('active');
		$('.fold-5').eq(0).addClass('active');
	}
	if(val >= 55 && val <= 57){
		$('.fold-5').removeClass('active');
		$('.fold-5').eq(1).addClass('active');
	}
	if(val >= 58 && val <= 60){
		$('.fold-5').removeClass('active');
		$('.fold-5').eq(2).addClass('active');
	}
	if(val >= 61 && val <= 63){
		$('.fold-5').removeClass('active');
		$('.fold-5').eq(3).addClass('active');
	}
	if(val >= 64 && val <= 66){
		$('.fold-5').removeClass('active');
		$('.fold-5').eq(4).addClass('active');
	}
	if(val >= 67 && val <= 69){
		$('.fold-5').removeClass('active');
		$('.fold-5').eq(5).addClass('active');
	}
	if(val >= 70 && val <= 72){
		$('.fold-5').removeClass('active');
		$('.fold-5').eq(6).addClass('active');
	}
	if(val >= 73 && val <= 75){
		$('.fold-5').removeClass('active');
		$('.fold-5').eq(7).addClass('active');
	}
	if(val >= 75 && val <= 78){
		$('.fold-5').removeClass('active');
		$('.fold-5').eq(8).addClass('active');
	}


	
	// MACHINE ANIMATION
	if(val <= 0){
		$('.machine').eq(0).addClass('active');
	}
	if(val >= 0 && val <= 3){
		$('.machine').removeClass('active');
		$('.machine').eq(0).addClass('active');
	}
	if(val >= 4 && val <= 6){
		$('.machine').removeClass('active');
		$('.machine').eq(1).addClass('active');
	}
	if(val >= 7 && val <= 9){
		$('.machine').removeClass('active');
		$('.machine').eq(2).addClass('active');
	}
	if(val >= 10 && val <= 12){
		$('.machine').removeClass('active');
		$('.machine').eq(3).addClass('active');
	}
	if(val >= 13 && val <= 15){
		$('.machine').removeClass('active');
		$('.machine').eq(4).addClass('active');
	}
	if(val >= 16 && val <= 18){
		$('.machine').removeClass('active');
		$('.machine').eq(5).addClass('active');
	}
	if(val >= 19 && val <= 21){
		$('.machine').removeClass('active');
		$('.machine').eq(6).addClass('active');
	}
	if(val >= 22 && val <= 24){
		$('.machine').removeClass('active');
		$('.machine').eq(7).addClass('active');
	}
	if(val >= 25 && val <= 27){
		$('.machine').removeClass('active');
		$('.machine').eq(8).addClass('active');
	}
	if(val >= 28 && val <= 30){
		$('.machine').removeClass('active');
		$('.machine').eq(9).addClass('active');
	}
	if(val >= 31 && val <= 33){
		$('.machine').removeClass('active');
		$('.machine').eq(10).addClass('active');
	}
	if(val >= 34 && val <= 36){
		$('.machine').removeClass('active');
		$('.machine').eq(11).addClass('active');
	}
	if(val >= 37 && val <= 39){
		$('.machine').removeClass('active');
		$('.machine').eq(12).addClass('active');
	}
	if(val >= 40 && val <= 42){
		$('.machine').removeClass('active');
		$('.machine').eq(13).addClass('active');
	}
	if(val >= 43 && val <= 45){
		$('.machine').removeClass('active');
		$('.machine').eq(14).addClass('active');
	}
	if(val >= 46 && val <= 48){
		$('.machine').removeClass('active');
		$('.machine').eq(15).addClass('active');
	}
	if(val >= 49 && val <= 51){
		$('.machine').removeClass('active');
		$('.machine').eq(16).addClass('active');
	}
	if(val >= 52 && val <= 54){
		$('.machine').removeClass('active');
		$('.machine').eq(17).addClass('active');
	}
	if(val >= 55 && val <= 57){
		$('.machine').removeClass('active');
		$('.machine').eq(18).addClass('active');
	}
	if(val >= 58 && val <= 60){
		$('.machine').removeClass('active');
		$('.machine').eq(19).addClass('active');
	}
	if(val >= 61 && val <= 63){
		$('.machine').removeClass('active');
		$('.machine').eq(20).addClass('active');
	}
	if(val >= 64 && val <= 66){
		$('.machine').removeClass('active');
		$('.machine').eq(21).addClass('active');
	}
	if(val >= 67 && val <= 69){
		$('.machine').removeClass('active');
		$('.machine').eq(22).addClass('active');
	}
	if(val >= 70 && val <= 72){
		$('.machine').removeClass('active');
		$('.machine').eq(23).addClass('active');
	}
	if(val >= 73 && val <= 75){
		$('.machine').removeClass('active');
		$('.machine').eq(24).addClass('active');
	}
	if(val >= 76 && val <= 78){
		$('.machine').removeClass('active');
		$('.machine').eq(25).addClass('active');
		$('.box').css({'left':158});
	}
	if(val >= 78){
		$('.machine').removeClass('active');
		$('.machine').eq(25).addClass('active');
		$('.box').css({'left':158});
	}
	
	
	// BOX 
	if(val >= 78 && val <= 81){
		$('.box').css({'left':168});
	}
	if(val >= 82 && val <= 84){
		$('.box').css({'left':178});
	}
	if(val >= 85 && val <= 87){
		$('.box').css({'left':188});
	}
	if(val >= 88 && val <= 90){
		$('.box').css({'left':198});
	}
	if(val >= 91 && val <= 93){
		$('.box').css({'left':208});
	}
	if(val >= 94 && val <= 96){
		$('.box').css({'left':218});
	}
	if(val >= 97 && val <= 99){
		$('.box').css({'left':228});
	}
	if(val >= 100 && val <= 102){
		$('.box').css({'left':238});
	}
	if(val >= 103 && val <= 105){
		$('.box').css({'left':248});
	}
	if(val >= 105){
		$('.box').css({'left':248});
	}
	
	
	//CAR
	if(val <= 0){
		$('.car').css({'left':590});
	}
	if(val >= 0 && val <= 3){
		$('.car').css({'left':592});
	}
	if(val >= 4 && val <= 6){
		$('.car').css({'left':594});
	}
	if(val >= 7 && val <= 9){
		$('.car').css({'left':596});
	}
	if(val >= 10 && val <= 12){
		$('.car').css({'left':598});
	}
	if(val >= 13 && val <= 15){
		$('.car').css({'left':600});
	}
	if(val >= 16 && val <= 18){
		$('.car').css({'left':602});
	}
	if(val >= 19 && val <= 21){
		$('.car').css({'left':604});
	}
	if(val >= 22 && val <= 24){
		$('.car').css({'left':606});
	}
	if(val >= 25 && val <= 27){
		$('.car').css({'left':608});
	}
	if(val >= 28 && val <= 30){
		$('.car').css({'left':610});
	}
	if(val >= 31 && val <= 33){
		$('.car').css({'left':612});
	}
	if(val >= 34 && val <= 36){
		$('.car').css({'left':614});
	}
	if(val >= 37 && val <= 39){
		$('.car').css({'left':616});
	}
	if(val >= 40 && val <= 42){
		$('.car').css({'left':618});
	}
	if(val >= 43 && val <= 45){
		$('.car').css({'left':620});
	}
	if(val >= 46 && val <= 48){
		$('.car').css({'left':622});
	}
	if(val >= 48){
		$('.car').css({'left':622});
	}

	
}
function fold_6_anim(val) {
	
		
	if(val <= 0){
		$('.fold-6').removeClass('active');
	}
	if(val >= 1 && val <= 3){
		$('.fold-6').removeClass('active');
		$('.fold-6').eq(0).addClass('active');
	}
	if(val >= 4 && val <= 6){
		$('.fold-6').removeClass('active');
		$('.fold-6').eq(1).addClass('active');
	}
	if(val >= 7 && val <= 9){
		$('.fold-6').removeClass('active');
		$('.fold-6').eq(2).addClass('active');
	}
	if(val >= 10 && val <= 77){
		$('.fold-6').removeClass('active');
		$('.fold-6').eq(3).addClass('active');
	}
	
	
	
	
	if(val >= 77 && val <= 79){
		$('.fold-6').removeClass('active');
		$('.fold-6').eq(4).addClass('active');
	}
	if(val >= 80 && val <= 82){
		$('.fold-6').removeClass('active');
		$('.fold-6').eq(5).addClass('active');
	}
	if(val >= 83 && val <= 85){
		$('.fold-6').removeClass('active');
		$('.fold-6').eq(6).addClass('active');
	}
	if(val >= 86 && val <= 87){
		$('.fold-6').removeClass('active');
		$('.fold-6').eq(7).addClass('active');
	}
	if(val >= 88 && val <= 90){
		$('.fold-6').removeClass('active');
		$('.fold-6').eq(8).addClass('active');
	}
	if(val >= 91 && val <= 93){
		$('.fold-6').removeClass('active');
		$('.fold-6').eq(9).addClass('active');
	}
	if(val >= 93 && val <= 96){
		$('.fold-6').removeClass('active');
		$('.fold-6').eq(10).addClass('active');
	}
	if(val >= 97 && val <= 99){
		$('.fold-6').removeClass('active');
		$('.fold-6').eq(11).addClass('active');
	}
	if(val >= 100 && val <= 102){
		$('.fold-6').removeClass('active');
		$('.fold-6').eq(12).addClass('active');
	}
	if(val >= 103 && val <= 105){
		$('.fold-6').removeClass('active');
		$('.fold-6').eq(13).addClass('active');
	}
	if(val >= 106 && val <= 108){
		$('.fold-6').removeClass('active');
		$('.fold-6').eq(14).addClass('active');
	}
	if(val >= 109 && val <= 111){
		$('.fold-6').removeClass('active');
		$('.fold-6').eq(15).addClass('active');
	}
	if(val >= 112 && val <= 114){
		$('.fold-6').removeClass('active');
		$('.fold-6').eq(16).addClass('active');
	}
	if(val >= 115 && val <= 117){
		$('.fold-6').removeClass('active');
		$('.fold-6').eq(17).addClass('active');
	}
	if(val >= 118 && val <= 120){
		$('.fold-6').removeClass('active');
		$('.fold-6').eq(18).addClass('active');
	}



	// CHART 1
	if(val <= 0){
		$('.chart-1').eq(0).addClass('active');
		$('.chart-1-wrap').css({'top':80});
	}
	if(val >= 0 && val <= 3){
		$('.chart-1').removeClass('active');
		$('.chart-1').eq(0).addClass('active');
		$('.chart-1-wrap').css({'top':80});
	}
	if(val >= 4 && val <= 6){
		$('.chart-1').removeClass('active');
		$('.chart-1').eq(1).addClass('active');
		$('.chart-1-wrap').css({'top':70});
	}
	if(val >= 7 && val <= 9){
		$('.chart-1').removeClass('active');
		$('.chart-1').eq(2).addClass('active');
		$('.chart-1-wrap').css({'top':60});
	}
	if(val >= 10 && val <= 12){
		$('.chart-1').removeClass('active');
		$('.chart-1').eq(3).addClass('active');
		$('.chart-1-wrap').css({'top':50});
	}
	if(val >= 13 && val <= 15){
		$('.chart-1').removeClass('active');
		$('.chart-1').eq(4).addClass('active');
		$('.chart-1-wrap').css({'top':40});
	}
	if(val >= 16 && val <= 18){
		$('.chart-1').removeClass('active');
		$('.chart-1').eq(5).addClass('active');
		$('.chart-1-wrap').css({'top':30});
	}
	if(val >= 19 && val <= 21){
		$('.chart-1').removeClass('active');
		$('.chart-1').eq(6).addClass('active');
		$('.chart-1-wrap').css({'top':20});
	}
	if(val >= 22 && val <= 24){
		$('.chart-1').removeClass('active');
		$('.chart-1').eq(7).addClass('active');
		$('.chart-1-wrap').css({'top':10});
	}
	if(val >= 25 && val <= 27){
		$('.chart-1').removeClass('active');
		$('.chart-1').eq(8).addClass('active');
		$('.chart-1-wrap').css({'top':0});
	}
	if(val >= 28 && val <= 30){
		$('.chart-1').removeClass('active');
		$('.chart-1').eq(9).addClass('active');
//		$('.chart-1-wrap').css({'top':0});
	}
	if(val >= 31 && val <= 33){
		$('.chart-1').removeClass('active');
		$('.chart-1').eq(10).addClass('active');
//		$('.chart-1-wrap').css({'top':-20});
	}
	if(val >= 34 && val <= 36){
		$('.chart-1').removeClass('active');
		$('.chart-1').eq(11).addClass('active');
	}
	if(val >= 37 && val <= 39){
		$('.chart-1').removeClass('active');
		$('.chart-1').eq(12).addClass('active');
	}
	if(val >= 40 && val <= 42){
		$('.chart-1').removeClass('active');
		$('.chart-1').eq(13).addClass('active');
	}
	if(val >= 43 && val <= 45){
		$('.chart-1').removeClass('active');
		$('.chart-1').eq(14).addClass('active');
	}
	if(val >= 46 && val <= 48){
		$('.chart-1').removeClass('active');
		$('.chart-1').eq(15).addClass('active');
	}
	if(val >= 49 && val <= 51){
		$('.chart-1').removeClass('active');
		$('.chart-1').eq(16).addClass('active');
	}
	if(val >= 52 && val <= 54){
		$('.chart-1').removeClass('active');
		$('.chart-1').eq(17).addClass('active');
	}
	if(val >= 55 && val <= 57){
		$('.chart-1').removeClass('active');
		$('.chart-1').eq(18).addClass('active');
	}
	if(val >= 58 && val <= 60){
		$('.chart-1').removeClass('active');
		$('.chart-1').eq(19).addClass('active');
	}
	if(val >= 61 && val <= 63){
		$('.chart-1').removeClass('active');
		$('.chart-1').eq(20).addClass('active');
	}
	if(val >= 64 && val <= 66){
		$('.chart-1').removeClass('active');
		$('.chart-1').eq(21).addClass('active');
	}
	if(val >= 67 && val <= 69){
		$('.chart-1').removeClass('active');
		$('.chart-1').eq(22).addClass('active');
	}
	if(val >= 70 && val <= 72){
		$('.chart-1').removeClass('active');
		$('.chart-1').eq(23).addClass('active');
	}
	if(val >= 72){
		$('.chart-1').removeClass('active');
		$('.chart-1').eq(23).addClass('active');
	}


	// CHART 2
	if(val <= 0){
		$('.chart-2').eq(0).addClass('active');
	}
	if(val >= 0 && val <= 3){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(0).addClass('active');
	}
	if(val >= 4 && val <= 6){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(1).addClass('active');
	}
	if(val >= 7 && val <= 9){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(2).addClass('active');
	}
	if(val >= 10 && val <= 12){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(3).addClass('active');
	}
	if(val >= 13 && val <= 15){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(4).addClass('active');
	}
	if(val >= 16 && val <= 18){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(5).addClass('active');
	}
	if(val >= 19 && val <= 21){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(6).addClass('active');
	}
	if(val >= 22 && val <= 24){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(7).addClass('active');
	}
	if(val >= 25 && val <= 27){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(8).addClass('active');
	}
	if(val >= 28 && val <= 30){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(9).addClass('active');
	}
	if(val >= 31 && val <= 33){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(10).addClass('active');
	}
	if(val >= 34 && val <= 36){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(11).addClass('active');
	}
	if(val >= 37 && val <= 39){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(12).addClass('active');
	}
	if(val >= 40 && val <= 42){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(13).addClass('active');
	}
	if(val >= 43 && val <= 45){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(14).addClass('active');
	}
	if(val >= 46 && val <= 48){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(15).addClass('active');
	}
	if(val >= 49 && val <= 51){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(16).addClass('active');
	}
	if(val >= 52 && val <= 54){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(17).addClass('active');
	}
	if(val >= 55 && val <= 57){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(18).addClass('active');
	}
	if(val >= 58 && val <= 60){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(19).addClass('active');
	}
	if(val >= 61 && val <= 63){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(20).addClass('active');
	}
	if(val >= 64 && val <= 66){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(21).addClass('active');
	}
	if(val >= 67 && val <= 69){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(22).addClass('active');
	}
	if(val >= 70 && val <= 72){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(23).addClass('active');
	}
	if(val >= 73 && val <= 75){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(24).addClass('active');
	}
	if(val >= 76 && val <= 78){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(25).addClass('active');
	}
	if(val >= 79 && val <= 81){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(26).addClass('active');
	}
	if(val >= 82 && val <= 84){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(27).addClass('active');
	}
	if(val >= 85 && val <= 87){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(28).addClass('active');
	}
	if(val >= 88 && val <= 90){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(29).addClass('active');
	}
	if(val >= 91 && val <= 93){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(30).addClass('active');
	}
	if(val >= 94 && val <= 96){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(31).addClass('active');
	}
	if(val >= 97 && val <= 99){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(32).addClass('active');
	}
	if(val >= 100 && val <= 102){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(33).addClass('active');
	}
	if(val >= 103 && val <= 105){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(34).addClass('active');
	}
	if(val >= 106 && val <= 108){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(35).addClass('active');
	}
	if(val >= 109 && val <= 111){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(36).addClass('active');
	}
	if(val >= 112 && val <= 114){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(37).addClass('active');
	}
	if(val >= 115 && val <= 117){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(38).addClass('active');
	}
	if(val >= 118 && val <= 120){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(39).addClass('active');
	}
	if(val >= 121 && val <= 123){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(40).addClass('active');
	}
	if(val >= 124 && val <= 126){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(41).addClass('active');
	}
	if(val >= 127 && val <= 129){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(42).addClass('active');
	}

	if(val >= 129){
		$('.chart-2').removeClass('active');
		$('.chart-2').eq(42).addClass('active');
	}
	
	// CHART 3
	if(val <= 0){
		$('.chart-3').eq(0).addClass('active');
	}
	if(val >= 1 && val <= 3){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(0).addClass('active');
	}
	if(val >= 4 && val <= 6){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(1).addClass('active');
	}
	if(val >= 7 && val <= 9){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(2).addClass('active');
	}
	if(val >= 10 && val <= 12){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(3).addClass('active');
	}
	if(val >= 13 && val <= 15){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(4).addClass('active');
	}
	if(val >= 16 && val <= 18){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(5).addClass('active');
	}
	if(val >= 19 && val <= 21){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(6).addClass('active');
	}
	if(val >= 22 && val <= 24){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(7).addClass('active');
	}
	if(val >= 25 && val <= 27){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(8).addClass('active');
	}
	if(val >= 28 && val <= 30){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(9).addClass('active');
	}
	if(val >= 31 && val <= 33){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(10).addClass('active');
	}
	if(val >= 34 && val <= 36){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(11).addClass('active');
	}
	if(val >= 37 && val <= 39){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(12).addClass('active');
	}
	if(val >= 40 && val <= 42){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(13).addClass('active');
	}
	if(val >= 43 && val <= 45){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(14).addClass('active');
	}
	if(val >= 46 && val <= 48){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(15).addClass('active');
	}
	if(val >= 49 && val <= 51){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(16).addClass('active');
	}
	if(val >= 52 && val <= 54){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(17).addClass('active');
	}
	if(val >= 55 && val <= 57){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(18).addClass('active');
	}
	if(val >= 58 && val <= 60){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(19).addClass('active');
	}
	if(val >= 61 && val <= 63){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(20).addClass('active');
	}
	if(val >= 64 && val <= 66){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(21).addClass('active');
	}
	if(val >= 67 && val <= 69){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(22).addClass('active');
	}
	if(val >= 70 && val <= 72){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(23).addClass('active');
	}
	if(val >= 73 && val <= 75){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(24).addClass('active');
	}
	if(val >= 76 && val <= 78){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(25).addClass('active');
	}
	if(val >= 79 && val <= 81){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(26).addClass('active');
	}
	if(val >= 82 && val <= 84){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(27).addClass('active');
	}
	if(val >= 85 && val <= 87){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(28).addClass('active');
	}
	if(val >= 88 && val <= 90){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(29).addClass('active');
	}
	if(val >= 91 && val <= 93){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(30).addClass('active');
	}
	if(val >= 94 && val <= 96){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(31).addClass('active');
	}
	if(val >= 97 && val <= 99){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(32).addClass('active');
	}
	if(val >= 100 && val <= 102){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(33).addClass('active');
	}
	if(val >= 103 && val <= 105){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(34).addClass('active');
	}
	if(val >= 106 && val <= 108){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(35).addClass('active');
	}
	if(val >= 109 && val <= 111){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(36).addClass('active');
	}
	if(val >= 112 && val <= 114){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(37).addClass('active');
	}
	if(val >= 115 && val <= 117){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(38).addClass('active');
	}
	if(val >= 118 && val <= 120){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(39).addClass('active');
	}
	if(val >= 121 && val <= 123){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(40).addClass('active');
	}

	if(val >= 123){
		$('.chart-3').removeClass('active');
		$('.chart-3').eq(40).addClass('active');
	}

}
function fold_7_anim(val) {
	
		
	if(val <= 0){
		$('.fold-7').removeClass('active');
	}
	if(val >= 85 && val <= 87){
		$('.fold-7').removeClass('active');
		$('.fold-7').eq(0).addClass('active');
	}
	if(val >= 88 && val <= 90){
		$('.fold-7').removeClass('active');
		$('.fold-7').eq(1).addClass('active');
	}
	if(val >= 91 && val <= 93){
		$('.fold-7').removeClass('active');
		$('.fold-7').eq(2).addClass('active');
	}
	if(val >= 94 && val <= 96){
		$('.fold-7').removeClass('active');
		$('.fold-7').eq(3).addClass('active');
	}
	if(val >= 97 && val <= 99){
		$('.fold-7').removeClass('active');
		$('.fold-7').eq(4).addClass('active');
	}
	if(val >= 100 && val <= 102){
		$('.fold-7').removeClass('active');
		$('.fold-7').eq(5).addClass('active');
	}
	if(val >= 103 && val <= 105){
		$('.fold-7').removeClass('active');
		$('.fold-7').eq(6).addClass('active');
	}
	if(val >= 106 && val <= 108){
		$('.fold-7').removeClass('active');
		$('.fold-7').eq(7).addClass('active');
	}
	if(val >= 109 && val <= 111){
		$('.fold-7').removeClass('active');
		$('.fold-7').eq(8).addClass('active');
	}
	if(val >= 112 && val <= 114){
		$('.fold-7').removeClass('active');
		$('.fold-7').eq(9).addClass('active');
	}
	if(val >= 115 && val <= 117){
		$('.fold-7').removeClass('active');
		$('.fold-7').eq(10).addClass('active');
	}
	if(val >= 118 && val <= 120){
		$('.fold-7').removeClass('active');
		$('.fold-7').eq(11).addClass('active');
	}
	if(val >= 121 && val <= 123){
		$('.fold-7').removeClass('active');
		$('.fold-7').eq(12).addClass('active');
	}
	if(val >= 124 && val <= 126){
		$('.fold-7').removeClass('active');
		$('.fold-7').eq(13).addClass('active');
	}
	if(val >= 127 && val <= 129){
		$('.fold-7').removeClass('active');
		$('.fold-7').eq(14).addClass('active');
	}
	if(val >= 130 && val <= 132){
		$('.fold-7').removeClass('active');
		$('.fold-7').eq(15).addClass('active');
	}
	if(val >= 133 && val <= 135){
		$('.fold-7').removeClass('active');
		$('.fold-7').eq(16).addClass('active');
	}
	if(val >= 136 && val <= 138){
		$('.fold-7').removeClass('active');
		$('.fold-7').eq(17).addClass('active');
	}
	if(val >= 139 && val <= 141){
		$('.fold-7').removeClass('active');
		$('.fold-7').eq(18).addClass('active');
	}
	if(val >= 142 && val <= 144){
		$('.fold-7').removeClass('active');
		$('.fold-7').eq(19).addClass('active');
	}
	if(val >= 145 && val <= 147){
		$('.fold-7').removeClass('active');
		$('.fold-7').eq(20).addClass('active');
	}
	if(val >= 148 && val <= 150){
		$('.fold-7').removeClass('active');
		$('.fold-7').eq(21).addClass('active');
	}


	
	
	// MANAGE 1
	if(val <= 0){
		$('.manage-1').eq(0).addClass('active');
	}
	if(val >= 1 && val <= 3){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(0).addClass('active');
	}
	if(val >= 4 && val <= 6){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(1).addClass('active');
	}
	if(val >= 7 && val <= 9){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(2).addClass('active');
	}
	if(val >= 10 && val <= 12){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(3).addClass('active');
	}
	if(val >= 13 && val <= 15){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(4).addClass('active');
	}
	if(val >= 16 && val <= 18){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(5).addClass('active');
	}
	if(val >= 19 && val <= 21){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(6).addClass('active');
	}
	if(val >= 22 && val <= 24){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(7).addClass('active');
	}
	if(val >= 25 && val <= 27){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(8).addClass('active');
	}
	if(val >= 28 && val <= 30){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(9).addClass('active');
	}
	if(val >= 31 && val <= 33){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(10).addClass('active');
	}
	if(val >= 34 && val <= 36){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(11).addClass('active');
	}
	if(val >= 37 && val <= 39){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(12).addClass('active');
	}
	if(val >= 40 && val <= 42){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(13).addClass('active');
	}
	if(val >= 43 && val <= 45){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(14).addClass('active');
	}
	if(val >= 46 && val <= 48){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(15).addClass('active');
	}
	if(val >= 49 && val <= 51){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(16).addClass('active');
	}
	if(val >= 52 && val <= 54){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(17).addClass('active');
	}
	if(val >= 55 && val <= 57){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(18).addClass('active');
	}
	if(val >= 58 && val <= 60){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(19).addClass('active');
	}
	if(val >= 61 && val <= 63){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(20).addClass('active');
	}
	if(val >= 64 && val <= 66){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(21).addClass('active');
	}
	if(val >= 67 && val <= 69){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(22).addClass('active');
	}
	if(val >= 70 && val <= 72){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(23).addClass('active');
	}
	if(val >= 73 && val <= 75){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(24).addClass('active');
	}
	if(val >= 76 && val <= 78){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(25).addClass('active');
	}
	if(val >= 79 && val <= 81){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(26).addClass('active');
	}
	if(val >= 82 && val <= 84){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(27).addClass('active');
	}
	if(val >= 85 && val <= 87){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(28).addClass('active');
	}
	if(val >= 88 && val <= 90){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(29).addClass('active');
	}
	if(val >= 91 && val <= 93){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(30).addClass('active');
	}
	if(val >= 94 && val <= 96){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(31).addClass('active');
	}
	if(val >= 97 && val <= 99){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(32).addClass('active');
	}
	if(val >= 100 && val <= 102){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(33).addClass('active');
	}
	if(val >= 103 && val <= 105){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(34).addClass('active');
	}
	if(val >= 106 && val <= 108){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(35).addClass('active');
	}
	if(val >= 109 && val <= 111){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(36).addClass('active');
	}
	if(val >= 112 && val <= 114){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(37).addClass('active');
	}
	if(val >= 115 && val <= 117){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(38).addClass('active');
	}
	if(val >= 118 && val <= 120){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(39).addClass('active');
	}
	if(val >= 121 && val <= 123){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(40).addClass('active');
	}
	if(val >= 124 && val <= 126){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(41).addClass('active');
	}
	if(val >= 127 && val <= 129){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(42).addClass('active');
	}
	if(val >= 130 && val <= 132){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(43).addClass('active');
	}
	if(val >= 133 && val <= 135){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(44).addClass('active');
	}
	if(val >= 136 && val <= 138){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(45).addClass('active');
	}
	if(val >= 139 && val <= 141){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(46).addClass('active');
	}
	if(val >= 142 && val <= 144){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(47).addClass('active');
	}
	if(val >= 145 && val <= 147){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(48).addClass('active');
	}
	if(val >= 148 && val <= 150){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(49).addClass('active');
	}

	if(val >= 150){
		$('.manage-1').removeClass('active');
		$('.manage-1').eq(49).addClass('active');
	}
	
	
	// MANAGE 2
	if(val <= 0){
		$('.manage-2').eq(0).addClass('active');
	}
	if(val >= 0 && val <= 3){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(0).addClass('active');
	}
	if(val >= 4 && val <= 6){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(1).addClass('active');
	}
	if(val >= 7 && val <= 9){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(2).addClass('active');
	}
	if(val >= 10 && val <= 12){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(3).addClass('active');
	}
	if(val >= 13 && val <= 15){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(4).addClass('active');
	}
	if(val >= 16 && val <= 18){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(5).addClass('active');
	}
	if(val >= 19 && val <= 21){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(6).addClass('active');
	}
	if(val >= 22 && val <= 24){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(7).addClass('active');
	}
	if(val >= 25 && val <= 27){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(8).addClass('active');
	}
	if(val >= 28 && val <= 30){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(9).addClass('active');
	}
	if(val >= 31 && val <= 33){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(10).addClass('active');
	}
	if(val >= 34 && val <= 36){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(11).addClass('active');
	}
	if(val >= 37 && val <= 39){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(12).addClass('active');
	}
	if(val >= 40 && val <= 42){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(13).addClass('active');
	}
	if(val >= 43 && val <= 45){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(14).addClass('active');
	}
	if(val >= 46 && val <= 48){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(15).addClass('active');
	}
	if(val >= 49 && val <= 51){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(16).addClass('active');
	}
	if(val >= 52 && val <= 54){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(17).addClass('active');
	}
	if(val >= 55 && val <= 57){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(18).addClass('active');
	}
	if(val >= 58 && val <= 60){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(19).addClass('active');
	}
	if(val >= 61 && val <= 63){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(20).addClass('active');
	}
	if(val >= 64 && val <= 66){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(21).addClass('active');
	}
	if(val >= 67 && val <= 69){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(22).addClass('active');
	}
	if(val >= 70 && val <= 72){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(23).addClass('active');
	}
	if(val >= 73 && val <= 75){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(24).addClass('active');
	}
	if(val >= 76 && val <= 78){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(25).addClass('active');
	}
	if(val >= 79 && val <= 81){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(26).addClass('active');
	}
	if(val >= 82 && val <= 84){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(27).addClass('active');
	}
	if(val >= 85 && val <= 87){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(28).addClass('active');
	}
	if(val >= 88 && val <= 90){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(29).addClass('active');
	}
	if(val >= 91 && val <= 93){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(30).addClass('active');
	}
	if(val >= 94 && val <= 96){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(31).addClass('active');
	}
	if(val >= 97 && val <= 99){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(32).addClass('active');
	}
	if(val >= 100 && val <= 102){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(33).addClass('active');
	}
	if(val >= 103 && val <= 105){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(34).addClass('active');
	}
	if(val >= 106 && val <= 108){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(35).addClass('active');
	}
	if(val >= 109 && val <= 111){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(36).addClass('active');
	}
	if(val >= 112 && val <= 114){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(37).addClass('active');
	}

	if(val >= 114){
		$('.manage-2').removeClass('active');
		$('.manage-2').eq(37).addClass('active');
	}
	
	
	// MANAGE 3
	if(val <= 0){
		$('.manage-3').eq(0).addClass('active');
	}
	if(val >= 0 && val <= 48){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(0).addClass('active');
	}
	if(val >= 49 && val <= 51){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(1).addClass('active');
	}
	if(val >= 52 && val <= 54){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(2).addClass('active');
	}
	if(val >= 55 && val <= 57){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(3).addClass('active');
	}
	if(val >= 58 && val <= 60){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(4).addClass('active');
	}
	if(val >= 61 && val <= 63){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(5).addClass('active');
	}
	if(val >= 64 && val <= 66){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(6).addClass('active');
	}
	if(val >= 67 && val <= 69){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(7).addClass('active');
	}
	if(val >= 70 && val <= 72){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(8).addClass('active');
	}
	if(val >= 73 && val <= 75){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(9).addClass('active');
	}
	if(val >= 76 && val <= 78){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(10).addClass('active');
	}
	if(val >= 79 && val <= 81){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(11).addClass('active');
	}
	if(val >= 82 && val <= 84){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(12).addClass('active');
	}
	if(val >= 85 && val <= 87){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(13).addClass('active');
	}
	if(val >= 88 && val <= 90){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(14).addClass('active');
	}
	if(val >= 91 && val <= 93){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(15).addClass('active');
	}
	if(val >= 94 && val <= 96){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(16).addClass('active');
	}
	if(val >= 97 && val <= 99){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(17).addClass('active');
	}
	if(val >= 100 && val <= 102){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(18).addClass('active');
	}
	if(val >= 103 && val <= 105){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(19).addClass('active');
	}
	if(val >= 106 && val <= 108){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(20).addClass('active');
	}
	if(val >= 109 && val <= 111){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(21).addClass('active');
	}
	if(val >= 112 && val <= 114){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(22).addClass('active');
	}
	if(val >= 115 && val <= 117){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(23).addClass('active');
	}
	if(val >= 118 && val <= 120){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(24).addClass('active');
	}
	if(val >= 121 && val <= 123){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(25).addClass('active');
	}
	if(val >= 124 && val <= 126){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(26).addClass('active');
	}
	if(val >= 127 && val <= 129){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(27).addClass('active');
	}
	if(val >= 130 && val <= 132){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(28).addClass('active');
	}
	if(val >= 133 && val <= 135){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(29).addClass('active');
	}
	if(val >= 136 && val <= 138){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(30).addClass('active');
	}
	if(val >= 139 && val <= 141){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(31).addClass('active');
	}
	if(val >= 142 && val <= 144){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(32).addClass('active');
	}
	if(val >= 145 && val <= 147){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(33).addClass('active');
	}

	if(val >= 147){
		$('.manage-3').removeClass('active');
		$('.manage-3').eq(33).addClass('active');
	}

	
	//$('.men-animation').show();
	//$('.fold-8').hide();

}
function fold_8_anim(val) {
	
		
	if(val <= 0){
		$('.fold-8').removeClass('active');
	}
	if(val >= 1 && val <= 3){
		$('.fold-8').removeClass('active');
		$('.fold-8').eq(0).addClass('active');
	}
	if(val >= 4 && val <= 6){
		$('.fold-8').removeClass('active');
		$('.fold-8').eq(1).addClass('active');
	}
	if(val >= 7 && val <= 9){
		$('.fold-8').removeClass('active');
		$('.fold-8').eq(2).addClass('active');
	}
	if(val >= 10 && val <= 12){
		$('.fold-8').removeClass('active');
		$('.fold-8').eq(3).addClass('active');
	}
	if(val >= 13 && val <= 15){
		$('.fold-8').removeClass('active');
		$('.fold-8').eq(4).addClass('active');
	}
	if(val >= 16 && val <= 18){
		$('.fold-8').removeClass('active');
		$('.fold-8').eq(5).addClass('active');
	}
	if(val >= 19 && val <= 21){
		$('.fold-8').removeClass('active');
		$('.fold-8').eq(6).addClass('active');
	}
	if(val >= 22 && val <= 24){
		$('.fold-8').removeClass('active');
		$('.fold-8').eq(7).addClass('active');
	}

	
	
	// MAP
	if(val <= 0){
		$('.map-1').eq(0).addClass('active');
	}
	if(val >= 0 && val <= 3){
		$('.map-1').removeClass('active');
		$('.map-1').eq(0).addClass('active');
	}
	if(val >= 4 && val <= 6){
		$('.map-1').removeClass('active');
		$('.map-1').eq(1).addClass('active');
	}
	if(val >= 7 && val <= 9){
		$('.map-1').removeClass('active');
		$('.map-1').eq(2).addClass('active');
	}
	if(val >= 10 && val <= 12){
		$('.map-1').removeClass('active');
		$('.map-1').eq(3).addClass('active');
	}
	if(val >= 13 && val <= 15){
		$('.map-1').removeClass('active');
		$('.map-1').eq(4).addClass('active');
	}
	if(val >= 16 && val <= 18){
		$('.map-1').removeClass('active');
		$('.map-1').eq(5).addClass('active');
	}
	if(val >= 19 && val <= 21){
		$('.map-1').removeClass('active');
		$('.map-1').eq(6).addClass('active');
	}
	if(val >= 22 && val <= 24){
		$('.map-1').removeClass('active');
		$('.map-1').eq(7).addClass('active');
	}
	if(val >= 25 && val <= 27){
		$('.map-1').removeClass('active');
		$('.map-1').eq(8).addClass('active');
	}
	if(val >= 28 && val <= 30){
		$('.map-1').removeClass('active');
		$('.map-1').eq(9).addClass('active');
	}
	if(val >= 31 && val <= 33){
		$('.map-1').removeClass('active');
		$('.map-1').eq(10).addClass('active');
	}
	if(val >= 34 && val <= 36){
		$('.map-1').removeClass('active');
		$('.map-1').eq(11).addClass('active');
	}
	if(val >= 37 && val <= 39){
		$('.map-1').removeClass('active');
		$('.map-1').eq(12).addClass('active');
	}
	if(val >= 40 && val <= 42){
		$('.map-1').removeClass('active');
		$('.map-1').eq(13).addClass('active');
	}
	if(val >= 43 && val <= 45){
		$('.map-1').removeClass('active');
		$('.map-1').eq(14).addClass('active');
	}
	if(val >= 46 && val <= 48){
		$('.map-1').removeClass('active');
		$('.map-1').eq(15).addClass('active');
	}
	if(val >= 49 && val <= 51){
		$('.map-1').removeClass('active');
		$('.map-1').eq(16).addClass('active');
	}
	if(val >= 52 && val <= 54){
		$('.map-1').removeClass('active');
		$('.map-1').eq(17).addClass('active');
	}
	if(val >= 55 && val <= 57){
		$('.map-1').removeClass('active');
		$('.map-1').eq(18).addClass('active');
	}
	if(val >= 58 && val <= 60){
		$('.map-1').removeClass('active');
		$('.map-1').eq(19).addClass('active');
	}
	if(val >= 61 && val <= 63){
		$('.map-1').removeClass('active');
		$('.map-1').eq(20).addClass('active');
	}

	if(val >= 63){
		$('.map-1').removeClass('active');
		$('.map-1').eq(20).addClass('active');
	}
	
	
	// PIN
	if(val <= 0){
		$('.pin').css({'top':-55,'opacity':0});
	}
	if(val >= 0 && val <= 63){
		$('.pin').css({'top':-45,'opacity':0});
	}
	if(val >= 64 && val <= 66){
		$('.pin').css({'top':-35,'opacity':0});
	}
	if(val >= 67 && val <= 69){
		$('.pin').css({'top':-25,'opacity':0.25});
	}
	if(val >= 70 && val <= 72){
		$('.pin').css({'top':-15,'opacity':0.5});
	}
	if(val >= 73 && val <= 75){
		$('.pin').css({'top':-5,'opacity':0.75});
	}
	if(val >= 76 && val <= 78){
		$('.pin').css({'top':5,'opacity':1});
	}
	if(val >= 79 && val <= 81){
		$('.pin').css({'top':15,'opacity':1});
	}
	if(val >= 82 && val <= 84){
		$('.pin').css({'top':25,'opacity':1});
	}
	if(val >= 85 && val <= 87){
		$('.pin').css({'top':35,'opacity':1});
	}
	if(val >= 88 && val <= 90){
		$('.pin').css({'top':45,'opacity':1});
	}
	if(val >= 91 && val <= 93){
		$('.pin').css({'top':55,'opacity':1});
	}
	if(val >= 94 && val <= 36){
		$('.pin').css({'top':65,'opacity':1});
	}
	if(val >= 97 && val <= 99){
		$('.pin').css({'top':75,'opacity':1});
	}
	if(val >= 100 && val <= 102){
		$('.pin').css({'top':85,'opacity':1});
	}
	if(val >= 103 && val <= 105){
		$('.pin').css({'top':95,'opacity':1});
	}
	if(val >= 106 && val <= 108){
		$('.pin').css({'top':105,'opacity':1});
	}
	if(val >= 109 && val <= 111){
		$('.pin').css({'top':115,'opacity':1});
	}
	if(val >= 112 && val <= 114){
		$('.pin').css({'top':125,'opacity':1});
	}
	if(val >= 115 && val <= 117){
		$('.pin').css({'top':135,'opacity':1});
	}
	if(val >= 118 && val <= 120){
		$('.pin').css({'top':145,'opacity':1});
	}
	if(val >= 121 && val <= 123){
		$('.pin').css({'top':155,'opacity':1});
	}
	
	if(val >= 123 && val <= 125){
		$('.pin').css({'top':165,'opacity':1});
	}
	if(val >= 126 && val <= 128){
		$('.pin').css({'top':175,'opacity':1});
	}
	if(val >= 129 && val <= 131){
		$('.pin').css({'top':185,'opacity':1});
	}
	if(val >= 132 && val <= 134){
		$('.pin').css({'top':195,'opacity':1});
	}
	if(val >= 135 && val <= 137){
		$('.pin').css({'top':197,'opacity':1});
	}
	
	if(val <= 137){
		$('.men-animation').show();
		$('.fold-9').css({'visibility':'hidden'});
	}
	
	if(val >= 137){
		$('.pin').css({'top':197,'opacity':1});
		$('.men-animation').hide();
		$('.fold-9').css({'visibility':'visible'});
	}

}
